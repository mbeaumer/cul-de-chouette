#!/bin/bash
cvlc ./musique.mp3 &
pid_t=$!

echo "processus VLC $!"


sleep 1
clear


lancerde() {
	somme=0
	declare -a VALEURDE
	if [ -n "$1" ]
	then
		if [ $1 -ge 1  ] && [ $1 -le 3  ]
		then
			for ((i=1;i<=$1;i++))
			do
				de=$((RANDOM%6+1))
				#echo "lancer de dé n°$i"
				#echo "-----------"
				VALEURDE[$i]=$de
				#echo "dé obtenu : ${VALEURDE[$i]}"
				somme=$(($somme+$de))
				#echo "total des dés: $somme"
				#echo
			done
			sommefinale=$somme
			#echo $sommefinale
			echo ${VALEURDE[*]}
		else
			echo "veuillez ajouter le nombre de dés à lancer en paramètre"
		fi
	else
		echo "entrez le nombre de dés à lancer en paramètre"
	fi
}

#la valeur retournée est un tableau, pour avoir toutes les valeurs des dés sous forme de tableau:
#${res[*]}
#pour un dé spécifique mettre entre crochet [le numéro de dé - 1] : 1er dé = [0]
#exemple: valeur du 3eme dé = ${res[2]}
#pour faire une somme il faut additioner  ${res[0]} + ${res[1]} ${res[2]}

#sous menu definissant l'ordre du joueur

#chaque joueur lance un dé, l'ordre de passage est defini en fonction su score de chacun (le plus petit commence)


function min {

mini=7
for ((i=1;i<=$1;i++))
do

if [ ${SCORE_NOM[i]} -lt $mini ]
then
         mini=${SCORE_NOM[i]}
         i_min=$i
fi
done

echo "Le minimum est ${JOUEUR_NOM[i_min]}"

}


function ordre_joueur {

#parametre 1 : nb de joueur
#parametre suivant : le nom des joueurs (p2 p3 p4 p5 ...)

JOUEUR_NOM=([1]=$2 [2]=$3 [3]=$4 [4]=$5 [5]=$6 [6]=$7)
SCORE_NOM=([1]=0 [2]=0 [3]=0 [4]=0 [5]=0 [6]=0)
for ((j=0;j<$1;j++))
do

echo -n "Appuyez sur entrée pour lancer le dé joueur ${JOUEUR_NOM[j+1]}"
stty cbreak         # or  stty raw
readchar=`dd if=/dev/tty bs=1 count=1 2>/dev/null`
stty -cbreak
declare -a VALEURDE
VALEURDE[0]=$(lancerde 1)
SCORE_NOM[j]=${VALEURDE[0]}
echo "Vous avez ${SCORE_NOM[j]}"

done

declare -a JOUEUR

for ((j=1;j<=$1;j++))
do
min $1
JOUEUR[j]=JOUEUR_NOM[i_min]
SCORE_NOM[i_min]=7
echo "Joueur $j est ${JOUEUR_NOM[i_min]}"
done
}


max(){
if [ "$1" -ge "$2" ] && [ "$1" -ge "$3" ]
then
	resmax=$1
elif [ "$2" -ge "$1" ] && [ "$2" -ge "$3" ]
then
        resmax=$2
else
        resmax=$3
fi
}


calcul_points(){
#calcul du resultat
sum12=$(($1+$2))
sum13=$(($1+$3))
sum23=$(($2+$3))
if [ $1 -eq $2 ] && [ $2 -eq $3 ]
then
	res="cul-de-chouette"
elif [ $sum12 -eq $3 ] || [ $sum13 -eq $2 ] || [ $sum23 -eq $1 ]
then
	res="velute"
elif [ $1 -eq $2 ] || [ $1 -eq $3 ] || [ $2 -eq $3 ]
then
	if [ $1 -eq $2 ]
	then
		 temp=$1
	else
		 temp=$3
	fi

	res="chouette"
else
	res="néant"
fi

echo "Coup joué : $res !"
#calcul des points en fonction du résultat

if [ $res = "chouette" ]
then
	points=$(($temp * $temp))

elif [ $res = "cul-de-chouette" ]
then
	if [ $1 -eq 1 ]
	then
	points=50
	elif [ $1 -eq 2 ]
	then
	points=60
	elif [ $1 -eq 3 ]
	then
	points=70
	elif [ $1 -eq 4 ]
	then
	points=80
	elif [ $1 -eq 5 ]
	then
	points=90
	elif [ $d1 -eq 6 ]
	then
	points=100
	fi

elif [ $res = "velute" ]
then
	max $1 $2 $3
	temp=$resmax
	points=$((2 * $temp * $temp))
else
	points=0
fi

echo "$points points !"

}


function affichage_menu {

echo "--------------------------------------------------------"
echo "|       Bienvenu(e) dans le jeu du cul de chouette     |"
echo "|                                                      |"
echo "|              1)  Lancer une partie                   |"
echo "|              2)  Consulter les règles du jeu         |"
echo "|              3)  Quitter le jeu                      |"
echo "--------------------------------------------------------"
}

affichage_menu


CHOIX='-1'
read -p "Quel est votre choix ? (Entrez un chiffre entre 1 et 3) : " CHOIX
while [ $CHOIX -ne '1' ] && [ $CHOIX -ne '2' ] && [ $CHOIX -ne '3' ]
do
        clear
        affichage_menu
        read -p "Quel est votre choix ? (Veuillez entrer un chiffre entre 1 et 3) : " CHOIX
done


if [ $CHOIX -eq "1" ]
then
echo "          Chargement de la partie"
echo "          saisir nombre joueurs et noms"
declare -a nom
read -r n
for ((i=0;i<$n;i++))
do
	read -r nom[$i]
done

ordre_joueur $n ${nom[*]}

score=([1]=0 [2]=0 [3]=0 [4]=0 [5]=0 [6]=0)

while [ ${score[1]} -lt 343 ] && [ ${score[2]} -lt 343 ] && [ ${score[3]} -lt 343 ] && [ ${score[4]} -lt 343 ] && [ ${score[5]} -lt 343 ] && [ ${score[6]} -lt 343 ]
do
	for ((k=1;k<=$n;k++))
	do
	echo "joueur $k lance les chouettes ! appuie sur entrée !"
	read -r
	nbpoints=$(lancerde 3)
	echo "joueur $k a obtenu les dés $nbpoints"
	calcul_points $nbpoints
	score[$k]=$((${score[$k]}+$points))
	if [ ${score[$k]} -ge 343 ]
	then
		gagnant=$k
	fi
	echo "appuie sur entrée pour joueur suivant"
	read -r
	done
	for ((k=1;k<=$n;k++))
	do
	echo "joueur $k a ${score[$k]}"
	echo
	done
done
echo "fin de la partie"
echo "Le gagnant est le joueur $gagnant"
#if [ ${score[1]} < ${score[2]} ]
#then max  ${score[1]}

fi

if [ $CHOIX -eq "2" ]
then

    clear
    cat README.md
    echo""
    echo""
    echo "---------------------------------------------------------------------------"
    SORTIE_OK='q'
    read -p "Tapez sur la touche 'Entrée' ou sur 'q' pour sortir du manuel  " SORTIE
    while [ $SORTIE != $SORTIE_OK ]
    do
        read SORTIE
    done
      clear
      ./chouette.sh
fi


if [ $CHOIX -eq "3" ]
then

   echo "         Au revoir ! Merci d'avoir joué. "
	 #echo "processus pid_t = $pid_t"
	 kill $pid_t
fi
