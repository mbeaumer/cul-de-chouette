# CUL DE CHOUETTE 

2 à 4 joueurs
3 dés

Initialisation de la partie :
Chaque joueur lance 1 dé, le plus petit commence.

Fin de la partie :
Le jeu s'arrête lorsqu'un joueur atteint 343 points 

## Valeur des points : 

### CHOUETTE :
paire de dés identiques
POINTS
carré de la chouette (chouette de 3 = 9 pts)

### VELUTE : 
l'addition de 2 dés est égale au 3ème
POINTS
double du carré de la velute (2, 3, 5 = velute de 5 = 50 pts)

### CHOUETTE VELUTE : 
Combinaison d'une chouette et d'une velute 
Le joueur doit obligatoirement crier "CHOUETTE VELUTE !!" Sinon il n'a que les points de la chouette.
POINTS : 
Idem Velute : double du carré de la velute. (2, 2, 4 = velute de 4 = 32)

### CUL DE CHOUETTE :
3 dés identiques
POINTS 
points fixes en fonction du cul : 
cul de 1 = 50 pts
cul de 2 = 60 pts
cul de 3 = 70 pts
cul de 4 = 80 pts
cul de 5 = 90 pts
cul de 6 = 100 pts

### SUITE : 
les valeurs des 3 dés se suivent
TOUS LES JOUEURS DOIVENT TAPER SUR LA TABLE ET CRIER 
"GRELOTTE CA PICOTE"
en cas d'égalité, les joueurs sont départagés par un 
"Sans fin, est la moisissure des bières bretonnes"
POINTS :
Le dernier à avoir réagi perd 10 pts

### SIROTAGE : 
en cas de chouette, le joueur peut rejouer un des dés
pour obtenir un cul de chouette.
Tous les joueurs parient sur la valeur du dé.
OPTIONNEL :
Chaque nombre est annoncé par les joueurs de la façon suivante :
Linotte (1), Alouette (2), Fauvette (3), Mouette (4), Bergeronnette (5), Chouette (6)
POINTS :
Si le joueur fait bien un cul de chouette, il gagne la valeur des points du cul, sinon, la valeur de sa chouette est soustraite de son score.
Si un joueur a gagné son pari, il gagne 25 pts.

### NEANT
Lors d’un Néant, le joueur gagne une Grelottine (sauf s’il en a déjà une). Si un des autres joueurs a une Grelotine, il peut alors l’utiliser pour défier le joueur !

### GRELOTTINE
Lorsque 2 joueurs ont une grelottine, l'un peut défier l'autre en criant « GRELOTTINE !! »
Le Grelottin choisit alors de mettre un certain nombre de points en jeu (au maximum 30% des points du plus petit score des deux joueurs). Puis, il le défie de faire une certaine combinaison (Chouette, Velute, Chouette velute, Cul de chouette, Suite). Si le défié réussi le défi, il gagne les points mis en jeu et les points de la combinaison, et le Grelottin perd les points mis en jeu. S’il ne réussit pas, il perd les points mis en jeu mais gagne les points de sa combinaison, alors que le Grelottin gagne les points mis en jeu.

### BEVUE

En cas de bévue (exemple : un joeur crie GRELOTTE CA PICOTE sans raison), le joueur (ou les joueurs) commettant la bévue se verra retirer 5 points de son score.


## BONUS
### Civet siroté

Il est possible de gagner un Civet en perdant un sirotage sur une Chouette de 6. 
Toutes les règles du sirotage restent applicables (il perd 36 points, et les autres joueurs parient sur le dés).

### Civet

Les joueurs peuvent utiliser leurs civets pour parier un certain nombre de points sur leur prochain coup (Chouette, Velute, Chouette Velute...). 
Dans le cas d’un Cul de chouette, il doit préciser avec ou sans sirotage.

En cas de succès, il gagne le nombre de points qu’il a misé, et les points gagnés par le coup. Dans le cas contraire, il perd les points mis en jeu, mais gagne les points qu’il a fait avec sa combinaison de dés (une velute par exemple).

Il est possible de miser au maximum, 102 points.
